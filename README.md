# Utiliser les volumes K8

------------

><img src="https://i.pinimg.com/280x280_RS/6b/68/be/6b68bed191fdd2fad36e4193e64764ee.jpg" width="50" height="50" alt="Carlin Fongang">

>Carlin FONGANG | fongangcarlin@gmail.com

>[LinkedIn](https://www.linkedin.com/in/carlinfongang/) | [GitLab](https://gitlab.com/carlinfongang) | [GitHub](https://github.com/carlinfongang) | [Credly](https://www.credly.com/users/carlin-fongang/badges)

_______


# Utilisation des volumes Kubernetes

Dans cette leçon, nous allons parler de l'utilisation des volumes Kubernetes. Voici un aperçu de ce que nous allons aborder :

1. **Volumes et volumeMounts**
2. **Partage de volumes entre conteneurs**
3. **Types de volumes courants**
4. **Démonstration pratique**

## Volumes et volumeMounts

Les volumes peuvent être configurés facilement dans la spécification de votre pod et de votre conteneur. Tout d'abord, nous devons spécifier nos volumes dans la spécification du pod. Ensuite, nous montons ces volumes sur nos conteneurs en utilisant la section volumeMounts dans la spécification du conteneur. Voici un exemple :

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: volume-pod
spec:
  containers:
  - name: my-container
    image: busybox
    volumeMounts: #<-- Une fois le volume spécifié, il peut être monté ainsi
    - name: my-volume #<-- cette valeur correspond au nom de volume spécifié dans la section "volumes"
      mountPath: /output
  volumes: #<-- Spécification de l'ensemble de volume
  - name: my-volume
    hostPath:
      path: /data
```

Dans cet exemple, nous avons un volume nommé "my-volume" qui est monté sur le conteneur à l'emplacement "/output".

## Partage de volumes entre conteneurs

Vous pouvez utiliser des volumes pour partager des données entre plusieurs conteneurs dans le même pod. Voici un exemple de configuration :

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: shared-volume-pod
spec:
  containers:
  - name: my-container1
    image: busybox
#--------------------------- 
    volumeMounts: #<-- Montage du volume pour le conteneur 1 
    - name: my-volume
      mountPath: /output #<-- Chemin différent de celui du conteneur 2
#---------------------------
  - name: my-container2
    image: busybox
#--------------------------- 
    volumeMounts: #<-- Montage du volume pour le conteneur 2
    - name: my-volume
      mountPath: /input #<-- Chemin différent de celui du conteneur 2
#--------------------------- 
  volumes:
  - name: my-volume
    emptyDir: {}
```

Dans cet exemple, les deux conteneurs partagent le volume "my-volume".

## Types de volumes courants

1. **hostPath** : Stocke les données dans un répertoire spécifié sur le nœud Kubernetes.
2. **emptyDir** : Crée un répertoire vide pour le stockage temporaire des données. Ce répertoire existe uniquement tant que le pod est en cours d'exécution.

## Démonstration pratique

```bash
ssh -i id_rsa user@PLUBLIC_IP
```

1. Créez un fichier de spécification de pod avec un volume `hostPath` :

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: volume-pod
spec:
  restartPolicy: Never #<-- Cette instruction ferra en sorte que le conteneur ne redemarra pas une fois la sauvegarde du fichier texte effectuée
  containers:
  - name: my-container
    image: busybox
    command: ["sh", "-c", "echo 'success' > /output/success.txt"] 
    volumeMounts:
    - name: my-volume
      mountPath: /output
  volumes:
  - name: my-volume
    hostPath:
      path: /var/data
```

>![Alt text](img/image.png)
*Création du pod avec montage de volume*


2. Appliquez la configuration et vérifiez le fichier sur le nœud :

```bash
kubectl get pod volume-pod -o wide
```

>![Alt text](img/image-1.png)

L'on peut constaté effectivement que l'opération à été compléte et le pod c'est arrêté

Une fois identifié le worker node sur lequel le volume à été monté, l'on vas se connecter dessus et intérroger le repertoire

```sh
cat /var/data/success.txt
```

>![Alt text](img/image-2.png)

L'on peut constater que le contenu du fichier à bien été sauvegardé.

3. Créez un pod avec un volume `emptyDir` partagé entre deux conteneurs :

```bash
nano shared-volumed-pod.yml
```

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: shared-volume-pod
spec:
  containers:
  - name: my-container1
    image: busybox
    command: ["sh", "-c", "while true; do echo 'success' > /output/output.txt; sleep 5; done"]
    volumeMounts:
    - name: my-volume
      mountPath: /output
  - name: my-container2
    image: busybox
    command: ["sh", "-c", "while true; do cat /input/output.txt; sleep 5; done"]
    volumeMounts:
    - name: my-volume
      mountPath: /input
  volumes:
  - name: my-volume
    emptyDir: {}
```

4. Appliquez la configuration et vérifiez les logs du deuxième conteneur :

```sh
kubectl create -f shared-volume-pod.yml
```

>![Alt text](img/image-3.png)


```bash
kubectl get pods
```

>![Alt text](img/image-4.png)
*Liste des pods en cours*

Nous allons ensuite vérifier que le second conteneur à bien exécuter la commande qui lui était dédiée, c'est à dire effectuer la lecture du fichier output.txt toutes les 5 secondes

```sh
kubectl logs shared-volume-pod -c my-container2
```
>![Alt text](img/image-5.png)

# Conclusion

Nous avons parler des volumes et des volumeMounts, du partage de volumes entre conteneurs, des types de volumes courants, et nous avons effectué une démonstration pratique de l'utilisation des volumes dans Kubernetes.



# Reférence


https://kubernetes.io/docs/concepts/storage/volumes/